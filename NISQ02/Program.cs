﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NISQ02
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            List<int[][]> boardList = new List<int[][]>();

            var numberOfTestStr = System.Console.ReadLine().Trim();
            int numberOfTests;
            Int32.TryParse(numberOfTestStr, out numberOfTests);

            int[][] nextBoardTemplate = {
                new int[] {0,0,0,0,0},
                new int[] {0,0,0,0,0},
                new int[] {0,0,0,0,0},
                new int[] {0,0,0,0,0},
                new int[] {0,0,0,0,0}
            };

            for (int i = 0; i < numberOfTests; i++)
            {
                string row_1 = System.Console.ReadLine().Trim();
                string row_2 = System.Console.ReadLine().Trim();
                string row_3 = System.Console.ReadLine().Trim();
                string row_4 = System.Console.ReadLine().Trim();
                string row_5 = System.Console.ReadLine().Trim();


                int[] row_1_arr = row_1.Split(',').Select(Int32.Parse).ToArray();
                int[] row_2_arr = row_2.Split(',').Select(Int32.Parse).ToArray();
                int[] row_3_arr = row_3.Split(',').Select(Int32.Parse).ToArray();
                int[] row_4_arr = row_4.Split(',').Select(Int32.Parse).ToArray();
                int[] row_5_arr = row_5.Split(',').Select(Int32.Parse).ToArray();

                int[][] board = {
                    row_1_arr,
                    row_2_arr,
                    row_3_arr,
                    row_4_arr,
                    row_5_arr
                };

                boardList.Add(board);
            }

            int[][] currentBoard;

            foreach(int[][] board in boardList){
                for (int i = 0; i < 100; i++)
                {
                    int[][] nextBoard = nextBoardTemplate;
                    if(i != 0){
                        currentBoard = nextBoard;    
                    }else{
                        currentBoard = board;
                    }

                    for (int row = 0; row < 5; row++)
                    {
                        for (int col = 0; col < 5; col++)
                        {
                            int countAlive = 0;

                            //Console.WriteLine($"Row: {getValue(row)}");
                            //Console.WriteLine($"Row + 1: {getValue(row + 1)}");
                            //Console.WriteLine($"Row - 1: {getValue(row - 1)}");
                            //Console.WriteLine($"Col: {getValue(col)}");
                            //Console.WriteLine($"Col + 1: {getValue(col + 1)}");
                            //Console.WriteLine($"Col - 1: {getValue(col - 1)}");

                            if (board[getValue(row + 1)][getValue(col)] == 1)
                                countAlive++;
                            
                            if (board[getValue(row - 1)][getValue(col)] == 1)
                                countAlive++;
                            
                            if (board[getValue(row)][getValue(col + 1)] == 1)
                                countAlive++;
                            
                            if (board[getValue(row)][getValue(col - 1)] == 1)
                                countAlive++;

                            if (board[getValue(row + 1)][getValue(col + 1)] == 1)
                                countAlive++;

                            if (board[getValue(row + 1)][getValue(col - 1)] == 1)
                                countAlive++;

                            if (board[getValue(row - 1)][getValue(col + 1)] == 1)
                                countAlive++;

                            if (board[getValue(row - 1)][getValue(col - 1)] == 1)
                                countAlive++;

                            if(countAlive == 3){
                                nextBoard[row][col] = 1;
                            }else{
                                nextBoard[row][col] = 0;
                            }

                        }
                    }
                    if (i == 100)
                    {
                        bool isAlive = false;
                        for (int row = 0; row < 5; row++)
                        {
                            for (int col = 0; col < 5; col++)
                            {
                                if (nextBoard[row][col] == 1)
                                    isAlive = true;
                            }
                        }
                        if (isAlive)
                            Console.WriteLine("YES");
                        else{
                            Console.WriteLine("NO");
                        }
                    }
                }
            }


            //System.Console.WriteLine(myInput);
        }
        public static int getValue(int i)
        {
            if (i == -1)
            {
                return 4;
            }
            else if (i == 5)
            {
                return 0;
            }
            else
            {
                return i;
            }
        }
    }
}
