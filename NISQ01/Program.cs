﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text;

//namespace NISQ01
//{
//    class MainClass
//    {
//        public static string ReverseString(string s)
//        {
//            char[] arr = s.ToCharArray();
//            Array.Reverse(arr);
//            return new string(arr);
//        }
//        public static void Main(string[] args)
//        {
//            bool keepRun = true;
//            while (keepRun)
//            {
//                var myInput = System.Console.ReadLine().Trim();

//                IDictionary firstRow = new Dictionary<int, string>();
//                firstRow.Add(1, " __ ");
//                firstRow.Add(2, " __ ");
//                firstRow.Add(3, " __");
//                firstRow.Add(4, " __ ");
//                firstRow.Add(5, " __");
//                firstRow.Add(6, " __");
//                firstRow.Add(7, " __ ");
//                firstRow.Add(8, "    ");
//                firstRow.Add(9, " ");
//                firstRow.Add(10, " _");
//                firstRow.Add(11, "   ");
//                firstRow.Add(12, "   ");
//                firstRow.Add(13, " __ ");
//                firstRow.Add(14, "    ");
//                firstRow.Add(15, " __ ");
//                firstRow.Add(16, " __ ");
//                firstRow.Add(18, " __ ");
//                firstRow.Add(19, " __ ");
//                firstRow.Add(20, "___");
//                firstRow.Add(21, "    ");
//                firstRow.Add(23, "    ");
//                firstRow.Add(25, "   ");
//                firstRow.Add(26, "__");
//                firstRow.Add(-32, "   ");

//                IDictionary secondRow = new Dictionary<int, string>();
//                secondRow.Add(1, "|__|");
//                secondRow.Add(2, "|__\\");
//                secondRow.Add(3, "|  ");
//                secondRow.Add(4, "|  \\");
//                secondRow.Add(5, "|_ ");
//                secondRow.Add(6, "|_ ");
//                secondRow.Add(7, "| _ ");
//                secondRow.Add(8, "|__|");
//                secondRow.Add(9, "|");
//                secondRow.Add(10, " |");
//                secondRow.Add(11, "|_/");
//                secondRow.Add(12, "|  ");
//                secondRow.Add(13, "|\\/|");
//                secondRow.Add(14, "|\\ |");
//                secondRow.Add(15, "|  |");
//                secondRow.Add(16, "|__|");
//                secondRow.Add(18, "|__|");
//                secondRow.Add(19, "|__ ");
//                secondRow.Add(20, " | ");
//                secondRow.Add(21, "|  |");
//                secondRow.Add(23, "|  |");
//                secondRow.Add(25, "\\_/");
//                secondRow.Add(26, " /");
//                secondRow.Add(-32, "   ");

//                IDictionary thirdRow = new Dictionary<int, string>();
//                thirdRow.Add(1, "|  |");
//                thirdRow.Add(2, "|__/");
//                thirdRow.Add(3, "|__");
//                thirdRow.Add(4, "|__/");
//                thirdRow.Add(5, "|__");
//                thirdRow.Add(6, "|  ");
//                thirdRow.Add(7, "|__|");
//                thirdRow.Add(8, "|  |");
//                thirdRow.Add(9, "|");
//                thirdRow.Add(10, "_|");
//                thirdRow.Add(11, "| \\");
//                thirdRow.Add(12, "|__");
//                thirdRow.Add(13, "|  |");
//                thirdRow.Add(14, "| \\|");
//                thirdRow.Add(15, "|__|");
//                thirdRow.Add(16, "|   ");
//                thirdRow.Add(18, "| \\ ");
//                thirdRow.Add(19, " __|");
//                thirdRow.Add(20, " | ");
//                thirdRow.Add(21, "|__|");
//                thirdRow.Add(23, "|/\\|");
//                thirdRow.Add(25, " | ");
//                thirdRow.Add(26, "/_");
//                thirdRow.Add(-32, "   ");

//                int printKind;
//                Int32.TryParse(myInput.Split(' ')[0], out printKind);

//                string printTxt = myInput.Substring(2);
//                printTxt = printTxt.ToUpper();

//                if (printKind == 1)
//                {
//                    foreach (char c in printTxt)
//                    {
//                        int index = char.ToUpper(c) - 64;
//                        Console.Write(firstRow[index]);
//                    }
//                    Console.WriteLine();
//                    foreach (char c in printTxt)
//                    {
//                        int index = char.ToUpper(c) - 64;
//                        Console.Write(secondRow[index]);
//                    }
//                    Console.WriteLine();
//                    foreach (char c in printTxt)
//                    {
//                        int index = char.ToUpper(c) - 64;
//                        Console.Write(thirdRow[index]);
//                    }
//                    Console.WriteLine();
//                }
//                else if (printKind == 2)
//                {
//                    foreach (char c in ReverseString(printTxt))
//                    {
//                        int index = char.ToUpper(c) - 64;
//                        StringBuilder rev = new StringBuilder();
//                        foreach (char revC in ReverseString(firstRow[index].ToString()))
//                        {
//                            char charToUse = revC;
//                            if (charToUse == '/')
//                                charToUse = '\\';
//                            else if (charToUse == '\\')
//                                charToUse = '/';

//                            rev.Append(charToUse);
//                        }
//                        Console.Write(rev.ToString());
//                    }
//                    Console.WriteLine();
//                    foreach (char c in ReverseString(printTxt))
//                    {
//                        int index = char.ToUpper(c) - 64;
//                        StringBuilder rev = new StringBuilder();
//                        foreach (char revC in ReverseString(secondRow[index].ToString()))
//                        {
//                            char charToUse = revC;
//                            if (charToUse == '/')
//                                charToUse = '\\';
//                            else if (charToUse == '\\')
//                                charToUse = '/';

//                            rev.Append(charToUse);
//                        }
//                        Console.Write(rev.ToString());
//                    }
//                    Console.WriteLine();
//                    foreach (char c in ReverseString(printTxt))
//                    {
//                        int index = char.ToUpper(c) - 64;
//                        StringBuilder rev = new StringBuilder();
//                        foreach (char revC in ReverseString(thirdRow[index].ToString()))
//                        {
//                            char charToUse = revC;
//                            if (charToUse == '/')
//                                charToUse = '\\';
//                            else if (charToUse == '\\')
//                                charToUse = '/';

//                            rev.Append(charToUse);
//                        }
//                        Console.Write(rev.ToString());
//                    }
//                    Console.WriteLine();
//                }else{
//                    keepRun = false;
//                }
//                //System.Console.WriteLine(myInput);
//            }
//        }
//    }
//}
