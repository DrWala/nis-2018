﻿using System;

namespace HackerTrail
{
    class Program
    {
        static void Main(string[] args)
        {
            bool canPrint = true;

            while (true)
            {
                var myInput = System.Console.ReadLine().Trim();
                int input;
                Int32.TryParse(myInput, out input);
                if (input == 42)
                    canPrint = false;
                if (canPrint)
                    System.Console.WriteLine(myInput);

            }
        }
    }
}
